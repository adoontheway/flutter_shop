import 'package:flutter/material.dart';
import 'package:flutter_shop/pages/index_page.dart';
import 'package:flutter_shop/provide/Counter.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/child_category.dart';
import 'package:flutter_shop/provide/category_goods_list.dart';
import 'package:flutter_shop/provide/details_info.dart';
import 'package:flutter_shop/provide/cart.dart';
import 'package:flutter_shop/provide/index.dart';
import 'package:flutter_shop/provide/left_category_nav.dart';

//路由注入引入以下文件
import 'package:fluro/fluro.dart';
import 'package:flutter_shop/routers/routers.dart';
import 'package:flutter_shop/routers/application.dart';

void main() {
  final providers = Providers()
    ..provide(Provider<Counter>.value(Counter(0)))
    ..provide(Provider<ChildCategory>.value(ChildCategory()))
    ..provide(
        Provider<CategoryGoodsListProvide>.value(CategoryGoodsListProvide()))
    ..provide(Provider<DetailsInfoProvide>.value(DetailsInfoProvide()))
    ..provide(Provider<CartProvide>.value(CartProvide()))
    ..provide(Provider<IndexPageProvide>.value(IndexPageProvide()))
    ..provide(Provider<LeftCategoryNavProvide>.value(LeftCategoryNavProvide()));
  runApp(
    ProviderNode(child: MyApp(), providers: providers),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //fluro路由的注入
    final router = Router();
    Routes.configureRouters(router);
    Application.router = router; //静态化

    return Container(
      child: MaterialApp(
        title: '百姓生活+',
        onGenerateRoute: Application.router.generator,
        //引入路由管理

        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: IndexPage(),
      ),
    );
  }
}
