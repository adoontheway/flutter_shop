import 'package:flutter/material.dart';
//import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemberPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('个人中心'),
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          _topHead(),
          _orderTitle(),
          _orderTypes(),
          _combineMyListTile(),
        ],
      ),
    );
  }

  //顶部用户头像和用户名显示
  Widget _topHead() {
    return Container(
//      width: ScreenUtil.getInstance().setWidth(750),
      padding: const EdgeInsets.all(20.0),
      color: Colors.pink,
      child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20),
            child: ClipOval(
              child: FadeInImage.assetNetwork(
//                width: ScreenUtil.getInstance().setWidth(200),
                width: 100,
                fit: BoxFit.fitWidth,
                image:
                    'https://avatars2.githubusercontent.com/u/20411648?s=460&v=4',
                placeholder: 'images/avatar.png',
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Text(
              '用户名',
              style: TextStyle(
                color: Colors.black,
//                fontSize: ScreenUtil.getInstance().setSp(36),
                fontSize: 24,
              ),
            ),
          ),
        ],
      ),
    );
  }

  //订单标题
  Widget _orderTitle() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: Colors.black12,
            width: 1.0,
          ),
        ),
      ),
      child: ListTile(
        leading: Icon(Icons.list),
        title: Text('我的订单'),
        trailing: Icon(Icons.navigate_next),
      ),
    );
  }

  //订单的类型
  Widget _orderTypes() {
    return Container(
      margin: const EdgeInsets.only(top: 5),
//      width: ScreenUtil.getInstance().setWidth(750),
//      height: ScreenUtil.getInstance().setHeight(150),
      padding: const EdgeInsets.all(10),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
//            width: ScreenUtil.getInstance().setWidth(187),
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.payment,
                  size: 30,
                ),
                Text('待付款'),
              ],
            ),
          ),
          Container(
//            width: ScreenUtil.getInstance().setWidth(187),
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.query_builder,
                  size: 30,
                ),
                Text('待付款'),
              ],
            ),
          ),
          Container(
//            width: ScreenUtil.getInstance().setWidth(187),
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.directions_car,
                  size: 30,
                ),
                Text('待收货'),
              ],
            ),
          ),
          Container(
//            width: ScreenUtil.getInstance().setWidth(187),
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.message,
                  size: 30,
                ),
                Text('待评价'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //通用ListTile
  Widget _myListTile(var leadIcon, String title) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(color: Colors.black12, width: 1.0),
        ),
      ),
      child: ListTile(
        leading: Icon(leadIcon),
        title: Text(title),
        trailing: Icon(Icons.navigate_next),
      ),
    );
  }

  //自定义组件组合
  Widget _combineMyListTile() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          _myListTile(Icons.favorite_border, '领取优惠券'),
          _myListTile(Icons.favorite_border, '已领取优惠券'),
          _myListTile(Icons.location_on, '地址管理'),
          _myListTile(Icons.phone_in_talk, '客服电话'),
          _myListTile(Icons.info_outline, '关于商城'),
        ],
      ),
    );
  }
}
