import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/index.dart';

import 'home_page.dart';
import 'category_page.dart';
import 'cart_page.dart';
import 'member_page.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  //数组存储底部导航栏按钮
  final List<BottomNavigationBarItem> bottomTabs = [
    BottomNavigationBarItem(
      icon: Icon(CupertinoIcons.home),
      title: Text('首页'),
    ),
    BottomNavigationBarItem(
      icon: Icon(CupertinoIcons.search),
      title: Text('分类'),
    ),
    BottomNavigationBarItem(
      icon: Icon(CupertinoIcons.shopping_cart),
      title: Text('购物车'),
    ),
    BottomNavigationBarItem(
      icon: Icon(CupertinoIcons.profile_circled),
      title: Text('会员中心'),
    ),
  ];

  //数组存储导航栏对应的路由页面
  final List<Widget> tabBodies = [
    HomePage(),
    CategoryPage(),
    CartPage(),
    MemberPage(),
  ];

  //定义目前的下标和对应的页面
//  int _currentIndex = 0;

//  var _currentPage;

  @override
  void initState() {
    super.initState();
//    _currentPage = tabBodies[_currentIndex];
  }

  @override
  Widget build(BuildContext context) {
    //设置适配尺寸 (填入设计稿中设备的屏幕尺寸) 假如设计稿是按iPhone6的尺寸设计的(iPhone6 750*1334)在这里全局适配
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Provide<IndexPageProvide>(builder: (context, child, indexProvide) {
      return Scaffold(
        backgroundColor: Color.fromRGBO(244, 245, 245, 1.0),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: indexProvide.currentIndex,
          items: bottomTabs,
          onTap: (index) {
            indexProvide.setCurrentIndex(index);
//          setState(() {
//            _currentIndex = index;
////            _currentPage = tabBodies[index];
//          });
          },
        ),
        body: IndexedStack(
          index: indexProvide.currentIndex,
          children: tabBodies,
        ),
      );
    });
  }
}
