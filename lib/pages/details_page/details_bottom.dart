import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/cart.dart';
import 'package:flutter_shop/provide/details_info.dart';
import 'package:flutter_shop/provide/index.dart';

class DetailsBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var goodsInfo =
        Provide.value<DetailsInfoProvide>(context).goodsInfo.data.goodInfo;
    var goodsId = goodsInfo.goodsId;
    var goodsName = goodsInfo.goodsName;
    var count = 1;
    var price = goodsInfo.presentPrice;
    var images = goodsInfo.image1;
    return Container(
      width: ScreenUtil.getInstance().setWidth(750),
      height: ScreenUtil.getInstance().setHeight(80),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          InkWell(
            onTap: () {
              Provide.value<IndexPageProvide>(context).setCurrentIndex(2);
              Navigator.pop(context);
            },
            child: Stack(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: ScreenUtil.getInstance().setWidth(110),
                  child: Icon(
                    Icons.shopping_cart,
                    size: 35,
                    color: Colors.pinkAccent,
                  ),
                ),
                Provide<CartProvide>(
                  builder: (context, child, cartInfo) {
                    String totalGoodsCount =
                        cartInfo.totalGoodsCount.toString();
                    return Positioned(
                      right: 10,
                      top: 0,
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(6, 3, 6, 3),
                        decoration: BoxDecoration(
                          color: Colors.pink,
                          border: Border.all(color: Colors.white, width: 2.0),
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        ),
                        child: Text(
                          totalGoodsCount,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil.getInstance().setSp(22),
                          ),
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
          InkWell(
            onTap: () async {
              await Provide.value<CartProvide>(context)
                  .save(goodsId, goodsName, count, price, images);
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil.getInstance().setWidth(320),
              height: ScreenUtil.getInstance().setHeight(80),
              color: Colors.green,
              child: Text(
                '加入购物车',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil.getInstance().setSp(28)),
              ),
            ),
          ),
          InkWell(
            onTap: () async {
              await Provide.value<CartProvide>(context).remove();
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil.getInstance().setWidth(320),
              color: Colors.pinkAccent,
              child: Text(
                '立即购买',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: ScreenUtil.getInstance().setSp(28),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
