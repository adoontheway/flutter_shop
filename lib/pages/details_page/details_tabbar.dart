import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/details_info.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailsTabBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provide<DetailsInfoProvide>(builder: (context, child, val) {
      return Container(
        margin: EdgeInsets.only(top: 15.0),
        child: Row(
          children: <Widget>[
            _myLeftTabBar(context, val.isLeft),
            _myRightTabBar(context, val.isRight),
          ],
        ),
      );
    });
  }

  //左侧TabBar
  Widget _myLeftTabBar(BuildContext context, bool isLeft) {
    return InkWell(
      onTap: () {
        Provide.value<DetailsInfoProvide>(context)
            .changeTabBarLeftOrRight('left');
      },
      child: Container(
        width: ScreenUtil.getInstance().setWidth(375),
        alignment: Alignment.center,
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: isLeft ? Colors.pinkAccent : Colors.black,
              width: 1.0,
            ),
          ),
        ),
        child: Text(
          '详情',
          style: TextStyle(color: isLeft ? Colors.pinkAccent : Colors.black),
        ),
      ),
    );
  }

  //右侧TabBar
  Widget _myRightTabBar(BuildContext context, bool isRight) {
    return InkWell(
      onTap: () {
        Provide.value<DetailsInfoProvide>(context)
            .changeTabBarLeftOrRight('right');
      },
      child: Container(
        width: ScreenUtil.getInstance().setWidth(375),
        alignment: Alignment.center,
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: isRight ? Colors.pinkAccent : Colors.black,
              width: 1.0,
            ),
          ),
        ),
        child: Text(
          '评论',
          style: TextStyle(color: isRight ? Colors.pinkAccent : Colors.black),
        ),
      ),
    );
  }
}
