import 'package:flutter/material.dart';
import 'package:flutter_shop/provide/details_info.dart';
import 'package:provide/provide.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailsWeb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var goodsDetail = Provide.value<DetailsInfoProvide>(context)
        .goodsInfo
        .data
        .goodInfo
        .goodsDetail;

    return Provide<DetailsInfoProvide>(builder: (context, child, val) {
      bool isLeft = val.isLeft;
      if (isLeft) {
        return Container(
          child: Html(data: goodsDetail),
        );
      } else {
        return Container(
          width: ScreenUtil.getInstance().setWidth(750),
          alignment: Alignment.center,
          padding: EdgeInsets.all(10.0),
          child: Text('暂时无数据'),
        );
      }
    });
  }
}
