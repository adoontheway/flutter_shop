import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shop/model/cartInfo.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/cart.dart';

class CartCount extends StatelessWidget {
  final CartInfoModel cartItem;

  const CartCount({Key key, this.cartItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(165),
      margin: const EdgeInsets.only(top: 5.0),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.black12),
      ),
      child: Row(
        children: <Widget>[
          _reduceButton(context),
          _countArea(context),
          _addButton(context),
        ],
      ),
    );
  }

  //减少按钮
  Widget _reduceButton(context) {
    return InkWell(
      onTap: () {
        Provide.value<CartProvide>(context)
            .addOrReduce(cartItem.goodsId, 'reduce');
      },
      child: Container(
        width: ScreenUtil.getInstance().setWidth(45),
        height: ScreenUtil.getInstance().setHeight(45),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: cartItem.count == 1 ? Colors.black12 : Colors.white,
            border: Border(
              right: BorderSide(width: 1.0, color: Colors.black12),
            )),
        child: cartItem.count == 1 ? Text('') : Text('-'),
      ),
    );
  }

  //加号按钮
  Widget _addButton(context) {
    return InkWell(
      onTap: () {
        Provide.value<CartProvide>(context)
            .addOrReduce(cartItem.goodsId, 'add');
      },
      child: Container(
        width: ScreenUtil.getInstance().setWidth(45),
        height: ScreenUtil.getInstance().setHeight(45),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              left: BorderSide(width: 1.0, color: Colors.black12),
            )),
        child: Text('+'),
      ),
    );
  }

  //数量显示区域
  Widget _countArea(context) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(70),
      height: ScreenUtil.getInstance().setHeight(45),
      alignment: Alignment.center,
      color: Colors.white,
      child: Text(cartItem.count.toString()),
    );
  }
}
