import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shop/model/cartInfo.dart';
import 'cart_count.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/cart.dart';
import 'package:flutter_shop/routers/application.dart';

class CartItem extends StatelessWidget {
  final CartInfoModel cartItem;

  const CartItem({Key key, this.cartItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
      padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1.0,
            color: Colors.black12,
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          _cartCheckButton(context),
          _cartImage(context),
          _cartName(),
          _cartPrice(context)
        ],
      ),
    );
  }

  //选择商品的按钮
  Widget _cartCheckButton(context) {
    return Container(
      alignment: Alignment.center,
      child: Checkbox(
        value: cartItem.isCheck,
        activeColor: Colors.pinkAccent,
        onChanged: (bool) {
          Provide.value<CartProvide>(context).changeCheckBox(cartItem.goodsId);
        },
      ),
    );
  }

  //商品图片
  Widget _cartImage(context) {
    return InkWell(
      onTap: () {
        Application.router.navigateTo(
          context,
          '/detail?id=${cartItem.goodsId}',
        );
      },
      child: Container(
        width: ScreenUtil.getInstance().setWidth(150),
        height: ScreenUtil.getInstance().setHeight(150),
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black12),
        ),
        child: Image.network(cartItem.images),
      ),
    );
  }

  //商品名称
  Widget _cartName() {
    return Container(
      width: ScreenUtil.getInstance().setWidth(300),
      padding: const EdgeInsets.all(10.0),
      alignment: Alignment.topLeft,
      child: Column(
        children: <Widget>[
          Text(cartItem.goodsName),
          CartCount(cartItem: cartItem),
        ],
      ),
    );
  }

  //商品价格
  Widget _cartPrice(context) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(150),
      alignment: Alignment.centerRight,
      child: Column(
        children: <Widget>[
          Text('￥${cartItem.price}'),
          Container(
            child: InkWell(
              onTap: () {
                Provide.value<CartProvide>(context)
                    .deleteOneGoods(cartItem.goodsId);
              },
              child: Icon(
                Icons.delete_forever,
                color: Colors.black26,
                size: 30,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
