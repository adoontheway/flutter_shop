import 'package:flutter/material.dart';
import '../model/category.dart';

class ChildCategory with ChangeNotifier {
  List<BxMallSubDto> childCategoryList = [];
  String categoryId = '4'; //大类ID用于切换小类列表
  int childIndex = 0; //子类索引
  String subId = ''; //子类ID

  int page = 1; //列表的页数
  String noMoreText = ''; //加载中无数据显示的text

  //大类切换逻辑
  setChildCategory(List<BxMallSubDto> list, String id) {
    childIndex = 0; //点击大类子类的索引置为0
    categoryId = id;
    //切换大类时初始化
    page = 1;
    noMoreText = '';

    //由于全部标签数据未给出，因此自己提前制作一个全部的标签
    BxMallSubDto all = BxMallSubDto();
    all.mallSubId = '';
    all.mallCategoryId = '00';
    all.comments = null;
    all.mallSubName = '全部';

    childCategoryList = [all];
    childCategoryList.addAll(list);

    notifyListeners();
  }

  //改变子类索引
  changChildIndex(int index, String id) {
    childIndex = index;
    subId = id;
    notifyListeners();
  }

  //增加Page的方法
  addPage() {
    page++;
  }

  //改变noMoreText的方法
  changeNoMoreText(String newNoMoreText) {
    noMoreText=newNoMoreText;
    notifyListeners();
  }
}
