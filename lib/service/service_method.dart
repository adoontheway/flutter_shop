import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:io';
import '../config/service_url.dart';

//抽出一个通用的方法
Future request(url,{fromData})async{
  print('开始获取数据....');
  try {
    Response response;
    Dio dio = Dio();
    dio.options.contentType =
        ContentType.parse('application/x-www-form-urlencoded');
    if(fromData!=null){
      response = await dio.post(servicePath[url], data: fromData);
    }else{
      response = await dio.post(servicePath[url]);
    }
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('后端接口出现异常');
    }
  }catch(e){
    return print('ERROR:======>$e');
  }

}


/*将请求抽取出来成为一个方法
Future getHomePageContent() async {//获取首页主体的内容
  print('开始获取首页数据....');
  try {
    Response response;
    Dio dio = Dio();
    dio.options.contentType =
        ContentType.parse('application/x-www-form-urlencoded');
    var fromData = {'lon': '115.02932', 'lat': '35.76189'};
    response = await dio.post(servicePath['homePageContent'], data: fromData);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('后端接口出现异常');
    }
  } catch (e) {
    return print('ERROR:======>$e');
  }
}


Future getHomePageBelowContent() async {//商城首页热卖商品（火爆专区的数据）
  print('开始获取首页火爆专区数据....');
  try {
    Response response;
    Dio dio = Dio();
    dio.options.contentType =
        ContentType.parse('application/x-www-form-urlencoded');
    int page=1;
    response = await dio.post(servicePath['homePageBelowConten'], data: page);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('后端接口出现异常');
    }
  } catch (e) {
    return print('ERROR:======>$e');
  }
}

*/