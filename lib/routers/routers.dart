import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter_shop/routers/route_handler.dart';

///总体路由配置
class Routes {
  static String root = '/'; //根目录
  static String detailsPage = '/detail'; //页面目录

  static void configureRouters(Router router) {
    router.notFoundHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, List<String>> params) {
        print('ERROR===>the route was not found');
      },
    );
    router.define(detailsPage, handler: detailsHandler);
  }
}
